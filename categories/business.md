---
layout: layouts/taxonomy.njk
title: Business / Investing
description: Posts from category Business / Investing
pagination:
  data: readyPosts.category.business
  size: 10
permalink: "category/business{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Category
---
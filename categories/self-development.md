---
layout: layouts/taxonomy.njk
title: Self Development
description: Posts from category Self Development
pagination:
  data: readyPosts.category.self-development
  size: 10
permalink: "category/self-development{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Category
---
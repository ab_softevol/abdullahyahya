---
layout: layouts/taxonomy.njk
title: web development
description: Posts from tag web development
pagination:
  data: readyPosts.tag.web-development-2
  size: 10
permalink: "tag/web-development-2{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Tag
---
---
layout: layouts/taxonomy.njk
title: islam
description: Posts from tag islam
pagination:
  data: readyPosts.tag.islam
  size: 10
permalink: "tag/islam{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Tag
---
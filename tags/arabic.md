---
layout: layouts/taxonomy.njk
title: arabic
description: Posts from tag arabic
pagination:
  data: readyPosts.tag.arabic
  size: 10
permalink: "tag/arabic{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Tag
---
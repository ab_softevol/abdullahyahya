---
layout: layouts/taxonomy.njk
title: Abdullah Yahya
description: Posts from author Abdullah Yahya
pagination:
  data: readyPosts.author.abdullah-yahya
  size: 10
permalink: "author/abdullah-yahya{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Author
---
---
layout: layouts/taxonomy.njk
title: Abdullah Yahya
description: Posts from author Abdullah Yahya
pagination:
  data: readyPosts.author.javanigus
  size: 10
permalink: "author/javanigus{% if pagination.pageNumber > 0 %}/{{ pagination.pageNumber | plus: 1 }}{% endif %}/"
taxonomy: Author
---